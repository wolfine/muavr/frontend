import { createStore } from 'vuex';
import axios from 'axios';
import auth from '@/store/auth';
import shop from '@/store/shop';

axios.defaults.baseURL = process.env.VUE_APP_BACKEND_URI || `api.${window.location.hostname}`;

export default createStore({
  modules: {
    auth,
    shop,
  },
});
