import axios from 'axios';

export default {
  namespaced: true,
  state() {
    return {
      authToken: null,
      refreshToken: null,
    };
  },
  actions: {
    async availableMethods() {
      return axios.get('/');
    },
  },
};
