import { createRouter, createWebHistory } from 'vue-router';
import store from '@/store';

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ '@/views/home'),
    meta: {
      authorizationRequired: false,
    },
  },
  {
    path: '/faq',
    name: 'faq',
    component: () => import(/* webpackChunkName: "faq", webpackPrefetch: true */ '@/views/faq'),
    meta: {
      authorizationRequired: false,
    },
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'not_found',
    component: () => import(/* webpackChunkName: "exception" */ '@/views/exception'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to) => {
  if (to.matched.some(route => route.meta.authorizationRequired)) {
    if (store.state.auth.accessToken) {
      return true;
    }
    return { name: 'home' };
  }

  return true;
});

export default router;
