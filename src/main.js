import { createApp } from 'vue';
import { VueReCaptcha } from 'vue-recaptcha-v3';
import ToastService from 'primevue/toastservice';
import PrimeVue from 'primevue/config';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '@/registerServiceWorker';
import '@/style/_icons.scss';
import '@/style/_layout.scss';
import '@/style/_helpers.scss';
import router from '@/router';
import store from '@/store';
import i18n, { getAvailableLocales, setI18nLanguage, loadLocale } from '@/i18n';
import app from './app.vue';

const vue = createApp(app)
  .use(PrimeVue, { ripple: true })
  .use(ToastService)
  .use(store)
  .use(VueReCaptcha, { siteKey: process.env.VUE_APP_GOOGLE_KEY })
  .use(router)
  .use(i18n);

vue.config.globalProperties.setLang = setI18nLanguage;
vue.config.globalProperties.getLocales = getAvailableLocales;
vue.config.globalProperties.loadLocale = loadLocale;

vue.mount('#app');
