import { createI18n } from 'vue-i18n';
import pluralizationRules from './pluralizationRules';

const currentLocale = window.localStorage.getItem('locale') || process.env.VUE_APP_DEFAULT_LOCALE;

window.localStorage.setItem('locale', currentLocale);

const messages = {};
// eslint-disable-next-line import/no-dynamic-require
messages[currentLocale] = require(`./source/${currentLocale}.json`);

const i18n = createI18n({
  locale: currentLocale,
  messages,
  fallbackLocale: process.env.VUE_APP_DEFAULT_LOCALE,
  pluralizationRules,
});

export default i18n;

export async function loadLocale(locale) {
  if (!i18n.global.availableLocales.includes(locale)) {
    const translation = await import(/* webpackChunkName: "locale-[request]" */ `./source/${locale}.json`);
    i18n.global.setLocaleMessage(locale, translation.default);
  }
}

export async function setI18nLanguage(locale) {
  await loadLocale(locale);

  if (i18n.mode === 'legacy') {
    i18n.global.locale = locale;
  } else {
    i18n.global.locale.value = locale;
  }

  document.querySelector('html').setAttribute('lang', locale);
  window.localStorage.setItem('locale', locale);
}

export function getAvailableLocales() {
  const result = [];

  require
    .context('./source', false, /\.json$/i, 'lazy')
    .keys()
    .forEach(fileName => {
      result.push(fileName.split('/')[1].replace(/(\.\/|\.json)/g, ''));
    });

  return result;
}
