user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
    multi_accept on;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    server_names_hash_bucket_size 128;
    server_name_in_redirect off;
    server_tokens off;

    keepalive_timeout 65;
    keepalive_requests 50;
    keepalive_disable msie6;

    client_body_buffer_size 1M;
    client_max_body_size 10M;
    client_header_buffer_size 1k;
    large_client_header_buffers 4 8k;

    reset_timedout_connection on;
    client_body_timeout 10;
    send_timeout 10;

    open_file_cache max=200000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 3;
    open_file_cache_errors on;
    if_modified_since before;

    ssl_session_cache shared:SSL:50m;
    ssl_session_timeout 1d;
    ssl_prefer_server_ciphers on;
    ssl_protocols TLSv1.1 TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES256-SHA384;
    ssl_ecdh_curve secp384r1;
    ssl_stapling on;
    ssl_stapling_verify on;

    gzip on;
    gzip_static on;
    gzip_disable "msie6";
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript;
    gzip_buffers 16 8k;
    gzip_comp_level 6;
    gzip_min_length 1024;
    gzip_vary on;
    gzip_proxied any;

    resolver 8.8.8.8 8.8.4.4 valid=300s;
    resolver_timeout 30s;

    add_header Strict-Transport-Security "max-age=31536000; includeSubdomains; preload";
    add_header X-XSS-Protection "1" always;
    add_header X-Frame-Options "sameorign" always;
    add_header X-Content-Type-Options "nosniff" always;

    expires 24h;
    etag on;

    index index.php index.html;
    charset utf-8;
    autoindex off;

    limit_rate_after 20m;
    limit_rate 500k;

    server {
        listen 80 default;
        listen [::]:80 default;

        server_name domain.loc;

        root /app;

        location / {
            try_files $uri $uri/ /;
        }

        error_page 403 500 502 503 @fallback;

        location @fallback {
            try_files $uri /500.html;
        }

        location /index.html {
            expires epoch;
        }
    }
}
