module.exports = {
  integrity: process.env.NODE_ENV !== 'development',
  productionSourceMap: false,
  runtimeCompiler: true,
  pwa: {
    themeColor: '#4DBA87',
    manifestOptions: {
      start_url: '/',
      dir: 'rtl',
      scope: './',
      orientation: 'portrait',
    },
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import '@/style/_layout';
        @import '@/style/diamond/layout/_mixins';
        @import "@/style/_theme";
        `,
      },
    },
  },
};
